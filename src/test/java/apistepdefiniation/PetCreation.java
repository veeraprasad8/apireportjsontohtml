package apistepdefiniation;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

public class PetCreation  {
    Response petResponse;

    @Given("^user building a post api with all the parameters$")
    public void user_building_a_post_api_with_all_the_parameters() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        FileInputStream fs = new FileInputStream(new File("C:\\Users\\veeraprasad_nalla\\eclipse-workspace\\myapidemo\\apidemo\\src\\test\\java\\resources\\EmployeeApis\\JsonpayloadS\\CreatPet.json"));

      petResponse =  RestAssured.given().headers("Content-Type","application/json").and().body(IOUtils.toString(fs,"UTF-8")).log().all().when().post("https://petstore.swagger.io/v2/pet");

    }

    @When("^user passing pet creation payload$")
    public void user_passing_pet_creation_payload() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
petResponse.prettyPrint();
    }

    @Then("^a new Pet should be created$")
    public void a_new_Pet_should_be_created() throws Throwable {
        // Write code here that turns the phrase above into concrete action
        System.out.println(petResponse.statusCode());
    }

}
