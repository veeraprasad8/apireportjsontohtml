Feature: Verify Employee Creation Post Api

 Scenario: Validate Employee Creation Functionality
   Given User provided all the inputs parameters for a post api
   When User Providing payload json and hitting post api
   """
   {"name":"nsegs","salary":"12220","age":"27"}
   """
   Then User should be able to get the new employee json response