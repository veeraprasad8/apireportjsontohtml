$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("CreateEmployee.feature");
formatter.feature({
  "line": 1,
  "name": "Verify Employee Creation Post Api",
  "description": "",
  "id": "verify-employee-creation-post-api",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Validate Employee Creation Functionality",
  "description": "",
  "id": "verify-employee-creation-post-api;validate-employee-creation-functionality",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User provided all the inputs parameters for a post api",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User Providing payload json and hitting post api",
  "keyword": "When ",
  "doc_string": {
    "content_type": "",
    "line": 6,
    "value": "{\"name\":\"nsegs\",\"salary\":\"12220\",\"age\":\"27\"}"
  }
});
formatter.step({
  "line": 9,
  "name": "User should be able to get the new employee json response",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateEmployee.user_provided_all_the_inputs_parameters_for_a_post_api()"
});
formatter.result({
  "duration": 598198200,
  "status": "passed"
});
formatter.match({
  "location": "CreateEmployee.user_Providing_payload_json_and_hitting_post_api(String)"
});
formatter.result({
  "duration": 1342194800,
  "status": "passed"
});
formatter.match({
  "location": "CreateEmployee.user_should_be_able_to_get_the_new_employee_json_response()"
});
formatter.result({
  "duration": 211460900,
  "status": "passed"
});
formatter.uri("CreatePet.feature");
formatter.feature({
  "line": 1,
  "name": "Verify Post Api of Create a new api",
  "description": "",
  "id": "verify-post-api-of-create-a-new-api",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Validate Pet creation in to the database using post api",
  "description": "",
  "id": "verify-post-api-of-create-a-new-api;validate-pet-creation-in-to-the-database-using-post-api",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "user building a post api with all the parameters",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "user passing pet creation payload",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "a new Pet should be created",
  "keyword": "Then "
});
formatter.match({
  "location": "PetCreation.user_building_a_post_api_with_all_the_parameters()"
});
formatter.result({
  "duration": 1470804100,
  "status": "passed"
});
formatter.match({
  "location": "PetCreation.user_passing_pet_creation_payload()"
});
formatter.result({
  "duration": 1541700,
  "status": "passed"
});
formatter.match({
  "location": "PetCreation.a_new_Pet_should_be_created()"
});
formatter.result({
  "duration": 86000,
  "status": "passed"
});
formatter.uri("Employees.feature");
formatter.feature({
  "line": 1,
  "name": "Validate All Employees Api Functionality",
  "description": "",
  "id": "validate-all-employees-api-functionality",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Validate Employees functionality",
  "description": "",
  "id": "validate-all-employees-api-functionality;validate-employees-functionality",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User provided all the inputs parameters for an api",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User hitting an employee get api",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "User should be able to get all the employee information and response schema also validated",
  "keyword": "Then "
});
formatter.match({
  "location": "EmployeesStepFile.user_provided_all_the_inputs_parameters_for_an_api()"
});
formatter.result({
  "duration": 109786900,
  "status": "passed"
});
formatter.match({
  "location": "EmployeesStepFile.user_hitting_an_employee_get_api()"
});
formatter.result({
  "duration": 1364200,
  "status": "passed"
});
formatter.match({
  "location": "EmployeesStepFile.user_should_be_able_to_get_all_the_employee_information_and_response_schema_also_validated()"
});
formatter.result({
  "duration": 271900,
  "status": "passed"
});
});